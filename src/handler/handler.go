package handler

import (
	"github.com/gin-gonic/gin"
)

var status, message string
var code = 200

func JSON(param map[string]interface{}, c *gin.Context) {
	message = ""
	status = ""

	var data = filterParam(param)

	if len(data) < 0 {
		data["response"] = "This action has no data"
	}

	var result = gin.H{
		"status":  status,
		"message": message,
		"result":  data,
	}

	c.JSON(code, result)
}

func Nodata(status, message string) map[string]interface{} {

	result := make(map[string]interface{})
	result["sttr"] = status
	result["msgr"] = message
	result["coder"] = 201

	return result
}
func filterParam(param map[string]interface{}) map[string]interface{} {
	if _, ok := param["sttr"]; ok {
		status = param["sttr"].(string)
		delete(param, "sttr")
	}

	if _, ok := param["msgr"]; ok {
		message = param["msgr"].(string)
		delete(param, "msgr")
	}

	if _, ok := param["coder"]; ok {
		code = param["coder"].(int)
		delete(param, "coder")
	}

	return param
}
