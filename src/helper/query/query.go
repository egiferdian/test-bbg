package query

import (
	db "config"
	"fmt"
	"strings"
)

func GetQuery(sqlString string) ([]map[string]interface{}, error) {

	con := db.CreateCon()
	rows, err := con.Query(sqlString)
	if err != nil {
		var dat []map[string]interface{}
		return dat, err
	}
	con.Close()

	columns, _ := rows.Columns()
	count := len(columns)
	tableData := make([]map[string]interface{}, 0)
	values := make([]interface{}, count)
	valuePtrs := make([]interface{}, count)

	for rows.Next() {
		for i := 0; i < count; i++ {
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...)
		entry := make(map[string]interface{})
		for i, col := range columns {
			var v interface{}
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}

			if val != nil {
				entry[col] = v
			} else {
				entry[col] = string("null")
			}
		}

		tableData = append(tableData, entry)
	}

	return tableData, nil
}

func GetOne(sqlString string) (map[string]interface{}, error) {

	qget, err := GetQuery(sqlString)
	if err != nil {
		panic(err)
	}
	tableData := make(map[string]interface{})
	if len(qget) > 0 {
		tableData = qget[0]
	}

	return tableData, nil
}

func UpdateSql(table string, data map[string]string, where string) bool {
	con := db.CreateCon()
	status := false
	if len(data) > 0 {
		qupdate := "UPDATE " + table
		qupdate += " SET "
		qupdate_value := ""
		for k, v := range data {
			str := v
			str = strings.Replace(str, "'", "''", -1)
			qupdate_value += "," + k + " = '" + str + "'"
		}
		qupdate_value = qupdate_value[1:]
		qupdate += qupdate_value

		qupdate += " WHERE " + where
		fmt.Println(qupdate)
		_, err := con.Exec(qupdate)
		status = true
		if err != nil {
			status = false
		}

	}

	con.Close()

	return status
}

func InsertSql(table string, data map[string]string) bool {
	con := db.CreateCon()
	status := false
	if len(data) > 0 {

		qupdate := "INSERT INTO " + table
		qupdate += " ( "
		qupdate_column := ""
		qupdate_value := ""
		for k, v := range data {
			qupdate_column += "," + k
			str := v
			str = strings.Replace(str, "'", "''", -1)
			qupdate_value += ",'" + str + "'"
		}
		qupdate_column = qupdate_column[1:]
		qupdate += qupdate_column + " )"

		qupdate += " VALUES ( "
		qupdate_value = qupdate_value[1:]
		qupdate += qupdate_value + " ) "
		fmt.Println(qupdate)
		_, err := con.Exec(qupdate)
		status = true
		if err != nil {
			status = false
		}

	}
	con.Close()

	return status
}
