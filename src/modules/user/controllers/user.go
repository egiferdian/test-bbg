package user

import (
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"handler"
	user_model "modules/user/models"
	"os"
	"time"

	_ "github.com/dgrijalva/jwt-go"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/sessions"
)

var (
	key   = []byte("super-secret-key")
	store = sessions.NewCookieStore(key)
)

var result = make(map[string]interface{})

func Login(c *gin.Context) {
	record := c.PostFormMap("data")
	passenc := sha1.New()
	decode_pass, _ := base64.StdEncoding.DecodeString(record["password"])
	record["password"] = string(decode_pass)

	passenc.Write([]byte(record["password"]))

	username := record["username"]
	password := passenc.Sum(nil)
	sha1pass := fmt.Sprintf("%x\n", password)

	data := user_model.GetUserLogin(username, sha1pass)

	token, _ := CreateToken(data["id"].(int64))
	result["token"] = token
	result["user_data"] = data

	if len(data) > 0 {
		result["status"] = "success"
	} else {
		result["status"] = "error"
	}

	handler.JSON(result, c)
}

func CreateToken(userId int64) (string, error) {
	var err error
	os.Setenv("ACCESS_SECRET", "jdnfksdmfksd")
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = userId
	atClaims["exp"] = time.Now().Add(time.Minute * 15).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return "", err
	}
	return token, nil
}
