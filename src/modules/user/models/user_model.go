package user_model

import (
	"helper/query"

	"helper/table"
	"strings"
)

func GetUserLogin(username string, password string) map[string]interface{} {
	data := make(map[string]interface{})
	sql := "SELECT * FROM " + table.Users + " WHERE username = '" + username + "' AND password = '" + strings.TrimSpace(password) + "' AND deleted_at IS NULL"
	row, _ := query.GetOne(sql)
	if row != nil && len(row) > 0 {
		for k, v := range row {
			data[k] = v
		}
	}

	return data
}
func GetUserByID(id string) map[string]interface{} {
	sql := "select u.id, u.fullname, u.username from " + table.Users + " u where u.id = '" + id + "' and u.deleted_at IS NULL "
	data, _ := query.GetOne(sql)

	return data
}
