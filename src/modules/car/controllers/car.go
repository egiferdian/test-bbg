package user

import (
	"fmt"
	"handler"
	car_model "modules/car/models"
	"time"

	"github.com/gin-gonic/gin"
)

var result = make(map[string]interface{})
var message, status string

func Index(c *gin.Context) {

	record := c.PostFormMap("data")

	keyword := record["keyword"]
	t_data := car_model.GetListCar(c, keyword)

	data_s := t_data
	temp_keyword := keyword

	result["data"] = data_s
	result["temp_keyword"] = temp_keyword

	handler.JSON(result, c)

}
func Detail(c *gin.Context) {
	idcar := c.Param("idcar")
	data := car_model.GetDetailCar(idcar)
	result["data"] = data

	handler.JSON(result, c)
}

func CreateCar(c *gin.Context) {

	record := c.PostFormMap("data")
	record["created_at"] = time.Now().Format("2006-01-02 15:04:05")

	fmt.Println(record)
	ok := car_model.CreateDataCar(record)
	if ok {
		status = "success"
		message = "Proses penambahan data mobil berhasil."
	} else {
		status = "error"
		message = "Proses penambahan data mobil gagal. Silahkan ulangi sekali lagi."
	}
	result = handler.Nodata(status, message)
	handler.JSON(result, c)
}

func Update(c *gin.Context) {

	record := c.PostFormMap("data")
	id_car := record["id"]
	record["updated_at"] = time.Now().Format("2006-01-02 15:04:05")
	ok := car_model.UpdateDataCar(record, id_car)

	if ok {
		status = "success"
		message = "Proses sunting data mobil berhasil."
	} else {
		status = "error"
		message = "Proses sunting data mobil gagal. Silahkan ulangi sekali lagi."
	}
	result = handler.Nodata(status, message)
	handler.JSON(result, c)
}

func Delete(c *gin.Context) {

	idcar := c.Param("idcar")

	ok := car_model.DeleteDataCar(c, idcar)
	if ok {
		status = "success"
		message = "Proses hapus data mobil berhasil."
	} else {
		status = "error"
		message = "Proses hapus data mobil gagal. Silahkan ulangi sekali lagi."
	}
	result = handler.Nodata(status, message)
	handler.JSON(result, c)
}
