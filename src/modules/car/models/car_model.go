package car_model

import (
	"helper/query"
	"helper/table"
	"time"

	"github.com/gin-gonic/gin"
)

func GetListCar(c *gin.Context, keyword string) []map[string]interface{} {
	querys := ""

	sql := "select c.id, c.name, c.image, c.brand, c.condition, c.price from " + table.Cars + " c"
	querys += " where c.deleted_at IS NULL"

	if len(keyword) > 0 {
		querys += " and (lower(c.name) like '%" + keyword + "%')"
	}

	sql += querys
	sql += " order by c.created_at"

	record, _ := query.GetQuery(sql)
	data := make([]map[string]interface{}, 0)
	if record != nil && len(record) > 0 {
		for _, v := range record {
			data = append(data, v)
		}
	}

	return data
}
func GetDetailCar(id string) map[string]interface{} {
	sql := "select c.id, c.name, c.price, c.brand, c.condition, c.qty, c.image, c.description, c.specification from " + table.Cars + " c where c.id = '" + id + "' and c.deleted_at IS NULL "
	data, _ := query.GetOne(sql)

	return data
}
func UpdateDataCar(record map[string]string, id_car string) (ret bool) {
	ret = false
	delete(record, "id")
	ok := query.UpdateSql(table.Cars, record, "id = '"+id_car+"'")

	if ok {
		ret = true
	}

	return ret
}
func CreateDataCar(record map[string]string) (ret bool) {
	ret = false
	ok := query.InsertSql(table.Cars, record)
	if ok {
		ret = true
	}

	return ret
}

func DeleteDataCar(c *gin.Context, idcar string) bool {
	record := make(map[string]string, 0)
	record["deleted_at"] = time.Now().Format("2006-01-02 15:04:05")

	ok := query.UpdateSql(table.Cars, record, "id = '"+idcar+"'")

	return ok
}
