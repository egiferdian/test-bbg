package main

import (
	"helper/security"
	car "modules/car/controllers"
	user "modules/user/controllers"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func Logout(c *gin.Context) {
	_, err := security.ExtractTokenMetadata(c.Request)
	if err != nil {
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	c.JSON(http.StatusOK, "Successfully logged out")
}

func main() {
	r := gin.Default()
	r.Use(cors.Default())
	r.Use(gin.Recovery())

	crs := r.Group("/cars")
	{
		// List & Search
		crs.POST("/index", car.Index)
		// Detail Data
		crs.GET("/detail/:idcar", car.Detail)
		// Create Data
		crs.POST("/create", security.TokenAuthMiddleware(), car.CreateCar)
		// Update Data
		crs.PUT("/update", security.TokenAuthMiddleware(), car.Update)
		// Delete Data
		crs.DELETE("/delete/:idcar", security.TokenAuthMiddleware(), car.Delete)
	}

	usr := r.Group("/users")
	{
		// Login User
		usr.POST("/login", user.Login)
		// Logout User
		usr.POST("/logout", security.TokenAuthMiddleware(), Logout)
	}

	r.Run(":443")
}
